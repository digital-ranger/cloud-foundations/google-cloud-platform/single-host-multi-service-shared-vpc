
/******************************************
	VPC configuration
 *****************************************/
resource "google_compute_network" "network" {
    name                            = var.network_name
    auto_create_subnetworks         = var.auto_create_subnetworks
    routing_mode                    = var.routing_mode
    project                         = var.project_id
    description                     = var.description
    delete_default_routes_on_create = var.delete_default_internet_gateway_routes
    mtu                             = var.mtu
}

/******************************************
	Shared VPC
 *****************************************/
resource "google_compute_shared_vpc_host_project" "shared_vpc_host" {
    count                           = var.shared_vpc_host ? 1 : 0
    project                         = var.project_id
    depends_on                      = [google_compute_network.network]
}

/******************************************
	Subnet configuration
 *****************************************/
resource "google_compute_subnetwork" "subnet" {
    name                            = var.subnetwork_name
    ip_cidr_range                   = var.subnet_ip_cidr_range
    region                          = var.region
    network                         = google_compute_network.network.id
    
}

/******************************************
	Service Project configuration
 *****************************************/
resource "google_compute_shared_vpc_service_project" "service_01" {
    host_project                    = google_compute_shared_vpc_host_project.shared_vpc_host[0].project
    service_project                 = var.service_project_id_01
}